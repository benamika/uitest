import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  items: Array<string> = ['Home', 'Workflow', 'Statistics', 'Calender', 'Users', 'Settings'];
  hightlightStatus: Array<boolean> = [];
  constructor() {
   }
  hightlightToogle(item): void {
    console.log('asd: ' + item);
   
  }
  ngOnInit() { }
}
