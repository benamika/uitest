import { EnttestPage } from './app.po';

describe('enttest App', function() {
  let page: EnttestPage;

  beforeEach(() => {
    page = new EnttestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
